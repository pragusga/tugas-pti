import { FC } from 'react';
import { Navbar, Nav, Form, FormControl } from 'react-bootstrap';
import { useForm } from 'react-hook-form';

export const NavbarElement: FC<{ getBookName: (bookName: string) => void }> = ({
  getBookName
}) => {
  const { register, handleSubmit } = useForm();
  const onSubmit = (data: any): void => {
    getBookName(data.nameBook);
    console.log('kirim nama buku');
  };

  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Nav className="mr-auto">
          <Navbar.Brand href="/">Library</Navbar.Brand>
        </Nav>
        <Form inline onSubmit={handleSubmit(onSubmit)}>
          <FormControl
            type="text"
            placeholder="Search"
            className="mr-sm-2"
            name="nameBook"
            ref={register}
          />
          <FormControl type="submit" />
        </Form>
      </Navbar>
    </>
  );
};
