import { FC } from 'react';
import { Card } from 'react-bootstrap';

interface CardProps {
  imgSrc: string;
  title: string;
}

export const CardElement: FC<CardProps> = ({ imgSrc, title }) => {
  return (
    <>
      <Card style={{ width: '18rem', marginBottom: '20px' }}>
        <Card.Img variant="top" src={imgSrc} />
        <Card.Body>
          <Card.Title>{title}</Card.Title>
        </Card.Body>
      </Card>
    </>
  );
};
