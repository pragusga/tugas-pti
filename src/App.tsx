import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { CardElement } from './components/Card';
import { NavbarElement } from './components/Navbar';

const App: React.FC = (): JSX.Element => {
  const [data, setData] = useState<any | null>(null);
  const [book, setBook] = useState<string>('python');

  const getBookName = (bookName: string): void => {
    setBook(bookName);
  };

  useEffect(() => {
    axios(
      `https://www.googleapis.com/books/v1/volumes?q=${book}&&maxResults=12`
    ).then((d) => {
      const { length } = d.data.items;
      if (length > 12) {
        setData(d.data.items);
        console.log(data);
      } else {
        setData(d.data.items);
        console.log(data, 'tessssss');
      }
    });
    console.log(data, 'tes');
  }, [book]);
  return (
    <>
      <NavbarElement getBookName={getBookName} />
      <div
        id="container"
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          flexWrap: 'wrap',
          backgroundColor: '#131313',
        }}
      >
        {data
          ? data.map((d: any, index: number) => (
              <CardElement
                title={d.volumeInfo.title}
                imgSrc={
                  d.volumeInfo.imageLinks
                    ? d.volumeInfo.imageLinks.thumbnail
                    : 'pou'
                }
                key={index}
              />
            ))
          : null}
      </div>
    </>
  );
};

export default App;
